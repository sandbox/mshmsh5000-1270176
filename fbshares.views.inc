<?php

/**
 * @file
 * Provide Views integration with Facebook Shares module.
 *
 * Our goal is just to make node queries sortable by number of FB shares, but
 * we also provide the share count as a field for convenience.
 */

/**
 * Implementation of hook_views_data().
 */
function fbshares_views_data() {
  $data = array();

  $data['fbshares_count']['table']['group'] = t('Facebook shares');

  $data['fbshares_count']['shares'] = array(
    'title' => t('Number of shares'),
    'help' => t('The number of Facebook shares for each node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['fbshares_count']['url'] = array(
    'title' => t('Facebook query URL'),
    'help' => t('The URL used to query the Facebook share count.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
  );
  
  // Join information.
  $data['fbshares_count']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );
  
  return $data;
}
