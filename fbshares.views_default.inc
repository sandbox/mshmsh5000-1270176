<?php

/**
 * @file
 * Default view to test FB shared listing logic.
 */

/**
 * Implementation of hook_views_default_views().
 */
function fbshares_views_default_views() {
  /*
   * View 'most_shared'
   */
  $view = new view;
  $view->name = 'most_shared';
  $view->description = 'Content most shared on Facebook.';
  $view->tag = 'social';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'shares' => array(
      'label' => 'Count',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'shares',
      'table' => 'fbshares_count',
      'field' => 'shares',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'shares' => array(
      'order' => 'DESC',
      'id' => 'shares',
      'table' => 'fbshares_count',
      'field' => 'shares',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'time',
    'results_lifespan' => '1800',
    'output_lifespan' => '1800',
  ));
  $handler->override_option('title', 'Most Shared');
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('block_description', 'Most shared - fbshare test');
  $handler->override_option('block_caching', -1);
  $views[$view->name] = $view;

  return $views;
}
